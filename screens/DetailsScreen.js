import React, { Component } from 'react';


class DetailsScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text> Touch Me!</Text>
            <Button
              title="Information"
              onPress={() => this.props.navigation.navigate('Profile')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default DetailsScreen;